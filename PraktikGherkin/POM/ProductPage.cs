﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace PraktikGherkin.POM
{
    public class ProductPage
    {
        private IWebDriver _driver;
        private Actions actions;
        public By ButtonBuy = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[5]/div[1]/button[1]");
        public ProductPage(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
    }
}
