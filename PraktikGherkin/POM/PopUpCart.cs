﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;


namespace PraktikGherkin.POM
{
    public class PopUpCart
    {
        private IWebDriver _driver;
        private Actions actions;
      
        public By TitleCart = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By TextLabeOnCartPopUp = By.XPath("/html/body/div[4]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By TextCartIsEmpty = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By ExitButton = By.XPath("/html/body/div[4]/div/div/div[1]/svg/use//svg/path");
        public By ButtonContinueSelectingItems = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/footer/button");
        public By supplementaryServicesProductInCart = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]/h4");
        public By buttonDeleteProductFromCart = By.ClassName("v-icon sprite-shared remove v-icon v-icon__close");

        public PopUpCart(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
    }
}
