﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraktikGherkin.POM
{
    public class EnterWithGoogleAccount
    {
        private IWebDriver _driver;
        public By TextVuberyAccaunt = By.XPath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[1]/div/h1/span");
        public By TextDiscription = By.XPath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[1]/div/div/span");
        public By FieldMyAccuntEmail = By.XPath("/html/body/div[1]/div[1]/div[2]/div/div[2]/" +
            "div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div/ul/li[2]/div");
        public By EnterGoogleEmail = By.XPath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[1]/div/div[1]/div");
        public By ButtonNext = By.XPath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button/span");
        public EnterWithGoogleAccount(IWebDriver driver)
        {
            this._driver = driver;
        }
    }
}
