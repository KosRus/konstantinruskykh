﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraktikGherkin.POM
{
  public  class Authorization
    {
        private IWebDriver _driver;
        public By ButtonVhod = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By ButtonRegistration = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[2]");
        public By FieldEmail = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By FieldPassword = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
        public By ButtonPasswordEye = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/span");
        public By ButtonEnter = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
        public By ButtonGoogle = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[3]/a[2]");
        public Authorization(IWebDriver driver)
        {
            this._driver = driver;
        }
    }
}

