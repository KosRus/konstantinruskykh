﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraktikGherkin.POM
{
    public class Header
    {
        private IWebDriver _driver;
        private Actions actions;
        public By Blog = By.LinkText("Блог");
        public By Fishka = By.LinkText("Fishka");
        public By Vakansii = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By Shops = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By DeliveryAndPayment = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By Credit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By WarrantyAndService = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By Contacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By SwitcherLanguageToogle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");
        public By SwitcherDarkLightToogle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");
        public By Geolocation = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By LiveSmart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a/p");
        public By AlloCash = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a/p");
        public By AlloUpgrade = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a/p");
        public By AlloChange = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a/p");
        public By Markdown = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a/p");
        public By Discounts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a/p");
        public By BurgerMenuCatalog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div");
        public By HeaderPhone = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[1]/span");
        public By SearchingInput = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By SearchingSubmitButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/button/svg");
        public By ButtonLoginOnTheHeader = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By ButtonRegistration = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[2]");
        public By ButtonCart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        public By MainLabelAllo = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By UserIsNotAuthentification = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div");
        public By TextLoginOnTheHeader = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/div[1]/span");
        public Header(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
    }
}
