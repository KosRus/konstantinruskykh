﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace PraktikGherkin.POM
{
   public class FishkaPage
    {
        private IWebDriver _driver;
        private Actions actions;

        public By TextFromFishkaPage = By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div[2]/div/div[1]/h1");
        public FishkaPage(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
    }
}
