﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace PraktikGherkin.POM
{
  public  class MainPage
    {
        private IWebDriver _driver;
        private Actions actions;
        public By nameOfProduct = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[4]/div[2]/div[1]/div[2]/div[2]/button");
        public By ButtonBuySecondItemOnMainPage = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[2]/button");
        public MainPage(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
    }
}
