﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PraktikGherkin.POM
{
   public class Blog
    {
        private IWebDriver _driver;
        public By TextArticlesFromBlog = By.XPath("/html/body/div[2]/header/div/div/div/div/div[2]/div[2]/nav/ul/li[2]/a");
        public Blog(IWebDriver driver)
        {
            this._driver = driver;
        }
    }
}
