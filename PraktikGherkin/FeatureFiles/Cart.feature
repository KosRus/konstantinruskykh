﻿Feature: Cart
	As a user
	I want to have access to my cart
	In order to view a contents of my order any time

Background: Opened Allo
	Given Allo website is open

Scenario: Unauthorized user has empry cart
	Given User is not logged in
	When User clicks on cart
	Then Cart is empty

Scenario: Unauthorized user save product in cart
	Given User is not logged in
	When User clicks on button add to product to cart
	When User clicks continue selecting item
	When User clicks on cart
	Then User see in cart product wich added

Scenario: Unauthorized user delete product from cart
	Given User is not logged in
	Given Product added to cart
	When User clicks continue selecting item
	When User clicks on cart
	When User clicks delete product from cart
	Then Cart is empty
