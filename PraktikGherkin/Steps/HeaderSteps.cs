﻿using PraktikGherkin.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;

namespace PraktikGherkin.Steps
{
    [Binding]
    public class HeaderSteps
    {   public Blog blog;
        public Authorization authorization;
        public ProductPage productPage;
        public Header header;
        public PopUpCart cart;
        public MainPage mainPage;
        public EnterWithGoogleAccount google;
        public IWebDriver driver;
        public Actions actions;
        public FishkaPage fishka;

        [Given(@"Allo website is opend")]
        public void GivenAlloWebsiteIsOpend()
        {
            driver = new ChromeDriver(@"C:\Users\MORENO\source\repos\PraktikGherkin\packages\Selenium.WebDriver.ChromeDriver.91.0.4472.10100\driver\win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
            header = new Header(driver);
            cart = new PopUpCart(driver);
            mainPage = new MainPage(driver);
            fishka = new FishkaPage(driver);
            blog = new Blog(driver);
        }

        [When(@"User clicks Blog")]
        public void WhenUserClicksBlog()
        {   
            driver.FindElement(header.Blog).Click();

        }

        [When(@"User clicks Fishka")]
        public void WhenUserClicksFishka()
        {
            driver.FindElement(header.Fishka).Click();
                
        }
        
        [Then(@"User on Blog page")]
        public void ThenUserOnBlogPage()
        {
            string result = driver.FindElement(blog.TextArticlesFromBlog).Text;
            Assert.AreEqual("Статті", result);
        }
        
        
        [Then(@"User on Fishka page")]
        public void ThenUserOnFishkaPage()
        {
            string result = driver.FindElement(fishka.TextFromFishkaPage).Text;
            Assert.AreEqual("Приємні винагороди за щоденні покупки", result);


        }
    }
}
