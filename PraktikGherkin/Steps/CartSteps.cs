﻿using PraktikGherkin.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;


namespace PraktikGherkin.Steps
{
    [Binding]
    public class CartSteps
    {
        public ProductPage productPage;
        public Header header;
        public PopUpCart cart;
        public MainPage mainPage;
        public IWebDriver driver;
        public Actions actions;





        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver = new ChromeDriver(@"C:\Users\MORENO\source\repos\PraktikGherkin\packages\Selenium.WebDriver.ChromeDriver.91.0.4472.10100\driver\win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
            header = new Header(driver);
            cart = new PopUpCart(driver);
            mainPage = new MainPage(driver);
        }

        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            string unlogged = driver.FindElement(header.ButtonLoginOnTheHeader).Text;
            Assert.AreEqual("Вхід", unlogged);
        }

        [When(@"User clicks on cart")]
        public void WhenUserClicksOnCart()
        {
            driver.FindElement(header.ButtonCart).Click();
        }

        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            string result = driver.FindElement(cart.TextCartIsEmpty).Text;
            Assert.AreEqual("Ваш кошик порожній.", result);
        }
        [When(@"User clicks on button add to product to cart")]
        public void WhenUserClicksOnButtonAddToProductToCart()
        {
            IWebElement button = driver.FindElement(mainPage.nameOfProduct);
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[4]/div[2]/div[1]")));
            actions.Perform();
            button.Click();
        }


        [When(@"User clicks continue selecting item")]
        public void WhenUserClicksContinueSelectingItem()
        {
            driver.FindElement(cart.ButtonContinueSelectingItems).Click();
        }

        [Then(@"User see in cart product wich added")]
        public void ThenUserSeeInCartProductWichAdded()
        {
            string itemInCart = driver.FindElement(cart.supplementaryServicesProductInCart).Text;
            Assert.AreEqual("Додаткові послуги", itemInCart);
        }
        [Given(@"Product added to cart")]
        public void GivenProductAddedToCart()
        {
            IWebElement button = driver.FindElement(mainPage.nameOfProduct);
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[4]/div[2]/div[1]")));
            actions.Perform();
            button.Click();
        }

        [When(@"User clicks delete product from cart")]
        public void WhenUserClicksDeleteProductFromCart()
        {
            IWebElement button = driver.FindElement(cart.buttonDeleteProductFromCart);
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[2]/div[1]/svg")));
            actions.Perform();
            button.Click();
        }

    }
}

