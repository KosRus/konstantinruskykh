﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace PraktikGherkin.Steps
{
 public static  class Hooks
    {
        public  static IWebDriver driver;
        [BeforeScenario]
        public static void BeforeScenario(IWebDriver driver)
        {
            driver = new ChromeDriver(@"C:\Users\MORENO\source\repos\PraktikGherkin\packages\Selenium.WebDriver.ChromeDriver.91.0.4472.10100\driver\win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
        }

        [AfterScenario]
        public static void AfterScenario(IWebDriver driver)
        {
            driver.Quit();
        }
    }
}
