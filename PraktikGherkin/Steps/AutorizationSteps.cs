﻿using PraktikGherkin.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using TechTalk.SpecFlow;

namespace PraktikGherkin.Steps
{
    [Binding]
    public class AutorizationSteps
    {
        public Authorization authorization;
        public ProductPage productPage;
        public Header header;
        public PopUpCart cart;
        public MainPage mainPage;
        public EnterWithGoogleAccount google;
        public IWebDriver driver;
        public Actions actions;


        [Given(@"Allo website is opened")]
        public void GivenAlloWebsiteIsOpened()
        {
            driver = new ChromeDriver(@"C:\Users\MORENO\source\repos\PraktikGherkin\packages\Selenium.WebDriver.ChromeDriver.91.0.4472.10100\driver\win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
            header = new Header(driver);
            authorization = new Authorization(driver);
            google = new EnterWithGoogleAccount(driver);


        }

        [Given(@"User is not logged")]
        public void GivenUserIsNotLogged()
        {
            string unlogged = driver.FindElement(header.ButtonLoginOnTheHeader).Text;
            Assert.AreEqual("Вхід", unlogged);
        }

        [When(@"User clicks on Login")]
        public void WhenUserClicksOnLogin()
        {
            driver.FindElement(header.ButtonLoginOnTheHeader).Click();
        }

        [When(@"User enter Phone or Email in the field")]
        public void WhenUserEnterPhoneOrEmailInTheField()
        {
            driver.FindElement(authorization.FieldEmail).SendKeys("russkih555@gmail.com");
        }

        [When(@"User enter password in the field")]
        public void WhenUserEnterPasswordInTheField()
        {
            driver.FindElement(authorization.FieldPassword).SendKeys("Manchester01");
        }

        [When(@"User click on button Enter")]
        public void WhenUserClickOnButtonEnter()
        {
            driver.FindElement(authorization.ButtonEnter).Click();
        }


        [When(@"User clicks the button google")]
        public void WhenUserClicksTheButtonGoogle()
        {
            driver.FindElement(authorization.ButtonGoogle).Click();
        }

        [When(@"User clicks enter email")]
        public void WhenUserClicksEnterEmail()
        {
            driver.FindElement(google.EnterGoogleEmail).SendKeys("russkih5555@gmail.com");
        }


        [Then(@"User authorized on the website")]
        public void ThenUserAuthorizedOnTheWebsite()
        {
            string result = driver.FindElement(header.TextLoginOnTheHeader).Text;
            Assert.AreEqual("Костянтин", result);
        }

    }
}
